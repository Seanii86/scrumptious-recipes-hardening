from django.urls import path


from meal_plans.views import (
    MealPlanListView,
    MealPlanDeleteView,
    MealPlanUpdateView,
    MealPlanDetailView,
    MealPlanCreateView,
)

urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plans_list"),
    path("create/", MealPlanCreateView.as_view(), name="meal_plans_new"),
    path("<int:pk>/", MealPlanDeleteView.as_view(), name="meal_plans_delete"),
    path("<int:pk>/edit/", MealPlanDetailView.as_view(), name="meal_plans_detail"),
    path("<int:pk>/delete/", MealPlanUpdateView.as_view(), name="meal_plans_edit"),

]
