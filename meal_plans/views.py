from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from meal_plans.models import MealPlan


class MealPlanListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    paginate_by = 2

    def get_queryset(self):
        return MealPlan.objects.filter(owner_id=self.request.user)


class MealPlanDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context["rating_form"] = RatingForm()
    #     return context


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    fields = ["name", "recipes", "date"]
    success_url = reverse_lazy("meal_plans_list")

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plans_detail", pk=plan.id)


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = ["name", "owner"]
    success_url = reverse_lazy("recipes_list")


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_list")







# from django.shortcuts import redirect
# from django.urls import reverse_lazy
# from django.views.generic import ListView
# from django.views.generic import DetailView
# from django.views.generic import CreateView, DeleteView, UpdateView
# from django.contrib.auth.mixins import LoginRequiredMixin
# from meal_plans.models import MealPlan


# class MealPlanListView(ListView):
#     model = MealPlan
#     template_name = "meal_plans/list.html"
#     paginate_by = 2


# class MealPlanCreateView(LoginRequiredMixin, CreateView):
#     model = MealPlan
#     template_name = "meal_plans/new.html"
#     fields = ["name", "date", "recipe", "owner"]
#     success_url = reverse_lazy("meal_plan_detail")

#     def form_valid(self, form):
#         plan = form.save(commit=False)
#         plan.instance.owner = self.request.user
#         plan.save()
#         form.save_m2m()
#         return redirect("meal_plan_detail", pk=plan.id)
